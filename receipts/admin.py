from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.


@admin.register(ExpenseCategory)
class ExpensCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
    )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "vendor",
    )
