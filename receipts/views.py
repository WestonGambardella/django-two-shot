from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def receipt_index(request):
    receipts_objects = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts_objects": receipts_objects}
    return render(request, "receipts/index.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipts = form.save(False)
            receipts.purchaser = request.user
            receipts.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/createcategory.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/createaccount.html", context)


@login_required
def category_index(request):
    category_objects = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_objects": category_objects,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_index(request):
    account_objects = Account.objects.filter(owner=request.user)
    context = {
        "account_objects": account_objects,
    }
    return render(request, "receipts/accounts.html", context)
