from django.urls import path
from receipts.views import (
    receipt_index,
    create_receipt,
    category_index,
    account_index,
    create_category,
    create_account,
)

urlpatterns = [
    path("", receipt_index, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_index, name="category_list"),
    path("accounts/", account_index, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
